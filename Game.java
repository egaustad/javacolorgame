/*Game Lab Client by Erik Gaustad*/

import java.text.*;
import java.util.Scanner;
import java.awt.*;
import java.awt.Component;
import javax.swing.*;
import java.awt.event.*;
import java.io.*;

public class Game extends JFrame implements ActionListener, KeyListener
{
   private GameBoardUltra GB1;
   private Container contents;
   private JPanel GameControls,GameInfo,dum1,dum2,dum3,dum4,dum5,Directions,Directions1,BottomDirections,HighScore,EasyPanel,DefaultPanel,HardPanel,BlankPanel,HScorePanel;
   private JButton up,down, left, right,Reset;
   private String score1, moves1, HScore1;
	private JLabel Welcome,Direct,Dir,Dir1,Score,ScoreNum,Time,TimeN,Empty1,Empty2,Empty3,Empty4,Empty5,Empty6,HScore,HScoreNum,SScore,HScoreBlank;
   private int num1, pointColor, total,num5,timeNum,HiScore,NumSquares;
   private Timer t = new Timer(50,this);
   private Timer t2 = new Timer(1000,this);
   private Color GiantsOrange = new Color(254,90,29);
   private Color darkGray = new Color(84,84,84);
   private JRadioButton Easy, Default, Hard;
   private ButtonGroup Dif;
   
   //instantiates a container along with calls to all the methods to create
   //a window with several panels to control and view the GameBoard class
   //also creates a panel to view the directions
   //all these panels are added to the Contents which is a container	
   public Game (String title)
   {
      super(title);
      contents = getContentPane();
      contents.setBackground(Color.black);
      contents.setLayout( new BorderLayout());
      //NumSquares = 15;
      //calls to the Directions method which creates a panel with all directions in it
      Directions();
      //creates the Game Info Panel which contains the GameControls panel 
      GameInfo();
		NumSquares = 15;
      //creates the GameBoard Panel at size 300
      GB1 = new GameBoardUltra(600,NumSquares);
		//GB1.ChangeDiff(15);
      GB1.setBackground(Color.black);
      TrackHighScore();
      
      HighScore();
      BottomDirections();
      //sets the location of the three panels with the BorderLayout
      contents.add(HighScore, BorderLayout.WEST);
      contents.add(BottomDirections, BorderLayout.SOUTH);
      contents.add(GB1, BorderLayout.CENTER);
      contents.add(Directions, BorderLayout.NORTH);
      contents.add(GameInfo, BorderLayout.EAST);
      num5 = 0;
      t.start();
      addKeyListener(this);
      setFocusable(true);
      setFocusTraversalKeysEnabled(false);
      timeNum = 30;       
      total = 0;
      //sets to visible and also sets window size
      setSize(864,968);
      setVisible( true );
   }
   public void TrackHighScore()
   {
      try
      {
         //Read in new file with scanner
         Scanner file = new Scanner(new File ("HighScore.txt"));
         //while statement for as long as the fild has next
         while (file.hasNext())
         {  
            //create new String to hold next line of the file
            String stringRead = file.nextLine();
            //new scanner to scan in line of text 

            HiScore = Integer.parseInt(stringRead);
         }
      } 
      //catch statement for filenotfoundexception to prevent program from crashing  
      catch (FileNotFoundException fnfeRef)
      {
         System.out.println("File not found");
      }
   
   
   }
   public void actionPerformed(ActionEvent e) 
   {
      if (e.getSource() == t)
      {
         if (!(timeNum==0))
         {
            if (num5 == 1)
            {
               num1 = GB1.MoveUp();
               if (num1 == 1)
               {
                  pointColor = GB1.getColor();
                  if (pointColor == 2)
                  {
                     timeNum+=3;
                  }
                  GB1.ColorSquare(0);   
               }
               else
               {
                  pointColor = 0;
               }
               total = total+pointColor;
            }
            else if (num5 == 2)
            {
               num1 = GB1.MoveDown();
               if (num1 == 1)
               {
                  pointColor = GB1.getColor();
                  if (pointColor == 2)
                  {
                     timeNum+=3;
                  }
                  GB1.ColorSquare(0);
                  
               }
               else
               {
                  pointColor = 0;
               }
               total = total+pointColor;
            }
            else if (num5 == 3)
            {
               num1 = GB1.MoveRight();
               if (num1 == 1)
               {
                  pointColor = GB1.getColor();
                  if (pointColor == 2)
                  {
                     timeNum+=3;
                  }
                  GB1.ColorSquare(0);
               }
               else
               {
                  pointColor = 0;
               }
               total = total+pointColor;
            }
            else if (num5 == 4)
            {
               num1 = GB1.MoveLeft();
               if (num1 == 1)
               {
                  pointColor = GB1.getColor();
                  if (pointColor == 2)
                  {
                     timeNum+=3;
                  }
                  GB1.ColorSquare(0);
               }
               else
               {
                  pointColor = 0;
               }
               total = total+pointColor;
            }
         ScoreNum.setText(""+total);
         if (total>HiScore)
         {
            HiScore = total;
            HScoreNum.setText(""+HiScore);
         }       
         }
      }
      else if (e.getSource() == t2)
      {
         if (!(timeNum==0))
         {
            timeNum--;
            TimeN.setText(""+timeNum);
         }
         else
         {
            t2.stop();
            GB1.stop();
            TimeN.setForeground(Color.red);
            TimeN.setFont(new Font("Times New Roman",Font.BOLD,20));
            TimeN.setText("GameOver!!");
            try
            {
               //crating an output stream to write to HighScore file
               FileOutputStream fos = new FileOutputStream("HighScore.txt", false);
               //PrintWriter to write to fos
               PrintWriter pw = new PrintWriter(fos);
               
               pw.print(HiScore);
               //close the printwriter so the file saves
               pw.close();
            }
            //catch statement to catch the filenotfoundexception to prevent crashing
            catch(FileNotFoundException fnfe)
            {
               System.out.println("Unable to find Reservation.txt");
            }
         }
      }
   }
   public void keyPressed(KeyEvent e) 
   {
      int code = e.getKeyCode();
      if (!(timeNum == 0))
      {
         if(code == KeyEvent.VK_UP) 
         {
            num1 = GB1.MoveUp();
            if (num1 == 1)
            {
               pointColor = GB1.getColor();
               if (pointColor == 2)
               {
                  timeNum+=3;
               }
               GB1.ColorSquare(0);
               t2.start();
               GB1.start();        
            }
            else
            {
               pointColor = 0;
            }
            total = total+pointColor;
            num5 = 1;
         }
         if(code == KeyEvent.VK_DOWN) 
         {
            num1 = GB1.MoveDown();
            if (num1 == 1)
            {
               pointColor = GB1.getColor();
               if (pointColor == 2)
               {
                  timeNum+=3;
               }
               GB1.ColorSquare(0);
               t2.start();
               GB1.start();
            }
            else
            {
               pointColor = 0;
            }
            total = total+pointColor;
            num5 = 2;
         }
         if(code == KeyEvent.VK_RIGHT) 
         {
            num1 = GB1.MoveRight();
            if (num1 == 1)
            {
               pointColor = GB1.getColor();
               if (pointColor == 2)
               {
                  timeNum+=3;
               }
               GB1.ColorSquare(0);
               t2.start();
               GB1.start(); 
            }
            else
            {
               pointColor = 0;
            }
            total = total+pointColor;
            num5 = 3;
         }
         if(code == KeyEvent.VK_LEFT) 
         {
            num1 = GB1.MoveLeft();
            if (num1 == 1)
            {
               pointColor = GB1.getColor();
               if (pointColor == 2)
               {
                  timeNum+=3;
               }
               GB1.ColorSquare(0);
               t2.start();
               GB1.start();
            }
            else
            {
               pointColor = 0;
            }
            total = total+pointColor;
            num5 = 4;
         }
      }
      ScoreNum.setText(""+total);
      if (total>HiScore)
      {
         HiScore = total;
         HScoreNum.setText(""+HiScore);
      }   
   }
   public void keyTyped(KeyEvent e) {}
   public void keyReleased(KeyEvent e){}
   //creates the GameInfo Panel which contains the GameControls....
   //along with the score and the amount of moves.
   //this panel is a gridlayout with 6 panels all in 1 column
   //a reset button is also created just above the arrows to reset the game
   //This Reset button uses the ResetButtonHandler class and actionListener 
   public void HighScore()
   {
      HighScore = new JPanel();
      HighScore.setLayout(new GridLayout(6,1));
      HighScore.setBackground(Color.black);
      HighScore.setPreferredSize(new Dimension(123,500));
      
      HScorePanel = new JPanel();
      HScorePanel.setLayout(new FlowLayout());
      HScorePanel.setBackground(Color.black);
      HScoreBlank = new JLabel("         ",SwingConstants.CENTER);
      HScoreBlank.setFont(new Font("Bodoni MT Black Italic",Font.BOLD,5));
      HScore = new JLabel("   High   ",SwingConstants.CENTER);
      SScore = new JLabel("   Score   ",SwingConstants.CENTER);
      SScore.setBackground(Color.black);
      HScore.setBackground(Color.black);
      HScore.setForeground(GiantsOrange);
      SScore.setForeground(GiantsOrange);
      HScore.setOpaque(true);
      HScore.setFont(new Font("Bookman Old Style",Font.BOLD,25));
      SScore.setFont(new Font("Bookman Old Style",Font.BOLD,25));
      HScorePanel.add(HScoreBlank);
      HScorePanel.add(HScore);
      HScorePanel.add(SScore);
      
      
      HScoreNum = new JLabel(""+HiScore,SwingConstants.CENTER);
      HScoreNum.setBackground(Color.black);
      HScoreNum.setForeground(Color.white);
      HScoreNum.setOpaque(true);
      HScoreNum.setFont(new Font("Times New Roman",Font.BOLD,50));
      
      BlankPanel = new JPanel();
      BlankPanel.setBackground(Color.black);
      BlankPanel.setLayout(new FlowLayout());
      
      EasyPanel = new JPanel();
      EasyPanel.setBackground(Color.black);
      EasyPanel.setLayout(new FlowLayout());
      Easy = new JRadioButton("Easy");
      Easy.setBackground(Color.black);
      Easy.setForeground(GiantsOrange);
      Easy.setFont(new Font("Times New Roman",Font.BOLD,20));
      
      DefaultPanel = new JPanel();
      DefaultPanel.setBackground(Color.black);
      DefaultPanel.setLayout(new FlowLayout());
		Default = new JRadioButton("Default");
      Default.setBackground(Color.black);
      Default.setForeground(GiantsOrange);
      Default.setFont(new Font("Times New Roman",Font.BOLD,20));
      
      HardPanel = new JPanel();
      HardPanel.setBackground(Color.black);
      HardPanel.setLayout(new FlowLayout());
		Hard = new JRadioButton("Hard");
      Hard.setBackground(Color.black);
      Hard.setForeground(GiantsOrange);
      Hard.setFont(new Font("Times New Roman",Font.BOLD,20));
      
      Dif = new ButtonGroup();
      Dif.add(Hard);
      Dif.add(Default);
      Dif.add(Easy);
      
      Easy.setFocusable(false);
      Default.setFocusable(false);
      Hard.setFocusable(false);
      
      Default.setSelected(true);
      RadioButtonHandler rbh = new RadioButtonHandler();
		Easy.addItemListener(rbh);
		Default.addItemListener(rbh);
		Hard.addItemListener(rbh);
		
      ImageIcon Flame = new ImageIcon("flame.jpg");
      JLabel Flame1 = new JLabel("", Flame, JLabel.CENTER);
      BlankPanel.add(Flame1);

      EasyPanel.add(Easy, SwingConstants.CENTER);
      DefaultPanel.add(Default, SwingConstants.CENTER);
      HardPanel.add(Hard, SwingConstants.CENTER);
      
      HighScore.add(HScorePanel);
      HighScore.add(HScoreNum);
      HighScore.add(BlankPanel);
      HighScore.add(EasyPanel);
      HighScore.add(DefaultPanel);
      HighScore.add(HardPanel);
      //HighScore.add(DifficultyList);
   } 
	public class RadioButtonHandler implements ItemListener
	{
		public void itemStateChanged(ItemEvent ie)
		{
			if (ie.getSource() == Easy)
			{
				GB1.ChangeDiff(10);
				
			}
			else if (ie.getSource() == Default)
			{
				GB1.ChangeDiff(15);
				
			}
			else if (ie.getSource() == Hard)
			{
				GB1.ChangeDiff(20);
				
			}
		}
	
	}
   public void BottomDirections()
   {
      BottomDirections = new JPanel();
      BottomDirections.setLayout(new FlowLayout());
      BottomDirections.setBackground(Color.black);
      BottomDirections.setPreferredSize(new Dimension(750,165));
      Empty4 = new JLabel("                                                                ");
      Empty5 = new JLabel("                                                                ");
      Empty6 = new JLabel("                                                                ");
      Empty4.setFont(new Font("Times New Roman",Font.BOLD,20));
      Empty5.setFont(new Font("Times New Roman",Font.BOLD,10));
      Empty6.setFont(new Font("Times New Roman",Font.BOLD,10));
      Direct = new JLabel("               Use the arrows to move the ball around the gameboard               ");
      Direct.setBackground(Color.black);
      Direct.setForeground(Color.white);
      //Direct.setOpaque(true);
      Direct.setFont(new Font("Times New Roman",Font.BOLD,20));
      Dir = new JLabel("               Your goal is to get as many points as possible in 30 seconds               ");
      Dir.setBackground(Color.black);
      Dir.setForeground(Color.white);
      //Dir.setOpaque(true);
      Dir.setFont(new Font("Times New Roman",Font.BOLD,20));
      Dir1 = new JLabel("Move to orange = +20, blue = +4, green = +3, yellow = +2 & +3 seconds, cyan = +1");
      Dir1.setBackground(Color.black);
      Dir1.setForeground(GiantsOrange);
      //Dir1.setOpaque(true);
      Dir1.setFont(new Font("Times New Roman",Font.BOLD,20));
      BottomDirections.add(Empty4);
      BottomDirections.add(Empty5);
      BottomDirections.add(Empty6);
      BottomDirections.add(Direct);
      BottomDirections.add(Dir);
      BottomDirections.add(Dir1);
   
   }
   public void GameInfo()
   {
      GameInfo = new JPanel();
      GameInfo.setLayout(new GridLayout(5,1));
      GameInfo.setBackground(Color.black);
      GameInfo.setPreferredSize(new Dimension(123,500));
      Score = new JLabel("Score",SwingConstants.CENTER);
      Score.setBackground(Color.black);
      Score.setForeground(Color.white);
      Score.setOpaque(true);
      Score.setFont(new Font("Times New Roman",Font.BOLD,30));
      score1 = "0";
      ScoreNum = new JLabel(score1,SwingConstants.CENTER);
      ScoreNum.setBackground(Color.black);
      ScoreNum.setForeground(GiantsOrange);
      ScoreNum.setOpaque(true);
      ScoreNum.setFont(new Font("Times New Roman",Font.BOLD,50));
      Time = new JLabel("Time",SwingConstants.CENTER);
      Time.setBackground(Color.black);
      Time.setForeground(Color.white);
      Time.setOpaque(true);
      Time.setFont(new Font("Times New Roman",Font.BOLD,30));
      moves1 = "30";
      TimeN = new JLabel(moves1,SwingConstants.CENTER);
      TimeN.setBackground(Color.black);
      TimeN.setForeground(GiantsOrange);
      TimeN.setOpaque(true);
      TimeN.setFont(new Font("Times New Roman",Font.BOLD,50));
      Reset = new JButton("New Game");
      ResetButtonHandler reset = new ResetButtonHandler();
      Reset.addActionListener(reset);
      Reset.setFocusable(false);
      GameInfo.add(Score);
      GameInfo.add(ScoreNum);
      GameInfo.add(Time);
      GameInfo.add(TimeN);
      GameInfo.add(Reset);  
   }
   //this method creates the Directions Panel which is flowLayout and..
   //Just gives directions typed out to user in different colors because
   //it is called the Color Game
   public void Directions()
   {
      Directions = new JPanel();
      ImageIcon image = new ImageIcon("giants.jpg");
      JLabel Giants = new JLabel("", image, JLabel.CENTER);
      Directions.setLayout(new BorderLayout());
      Directions.setBackground(Color.black);
      Directions.setPreferredSize(new Dimension(750,165));
      Directions1 = new JPanel();
      Directions1.setLayout(new FlowLayout());
      Directions1.setBackground(Color.black);
      Empty3 = new JLabel("                                                                                       ");
      Empty1 = new JLabel("                                                                                       ");
      Empty2 = new JLabel("                                                                                       ");
      Empty1.setFont(new Font("Times New Roman",Font.BOLD,17));
      Empty2.setFont(new Font("Times New Roman",Font.BOLD,8));
      Empty3.setFont(new Font("Times New Roman",Font.BOLD,10));
      Welcome = new JLabel("                      Giants Color Game                       ");
      Welcome.setBackground(Color.black);
      Welcome.setForeground(GiantsOrange);
      Welcome.setOpaque(true);
      Welcome.setFont(new Font("Bookman Old Style",Font.BOLD,65));
      Directions1.add(Empty1);
      Directions1.add(Empty2);
      Directions1.add(Empty3);
      Directions1.add(Welcome);
      Directions.add(Giants,BorderLayout.WEST);
      Directions.add(Directions1);
   } 
   //resetButtonHandler class to reset everything when the reset button is clicked
   //GB1 reset method is called to set the array values to 0 and redraw the board and marker 
   //setText is also set back to 0 
   private class ResetButtonHandler implements ActionListener
   { 
      public void actionPerformed( ActionEvent e)
      { 
         if (e.getSource() == Reset)
         {
            GB1.Reset();
            total = 0;
            timeNum = 30;
            ScoreNum.setText(""+total);   
            TimeN.setText(""+timeNum);
            num5 = 0;
            GB1.stop();
            t2.stop();
            TimeN.setFont(new Font("Times New Roman",Font.BOLD,50));
            TimeN.setForeground(GiantsOrange);
            //GB1.ChangeDiff(NumSquares);
         }
      }
   }
   public static void main(String[] args)
   {
      Game frame = new Game("Game");
      frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
   }
}