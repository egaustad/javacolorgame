# README #

### What is this repository for? ###

* This was a project for my intro to computer programming at PLNU
* It is a game to played on your computer which is written in Java.  All graphics are Java Based.
* See if you can beat my high score.  Mine is 1147.

### How do I get set up? ###

* Have all the files in the same directory and run the "Game" file.

### Who do I talk to? ###

* Erik Gaustad is the owner and admin of this repo and game.  Please email me at egaustad033@pointloma.edu with any questions.