/* GameBoard Class by
     Gaustad, Erik  */

import java.awt.*;
import javax.swing.*;
import java.util.Random;
//import java.util.System;
import java.awt.event.*;
//GameBoardUltra class that extends GameBoard 
//the GameBoard brings in the size from client
public class GameBoardUltra extends GameBoard implements ActionListener
{
   private int num1, ran1, ran2, numS, bSize,ran3,ran4,ran5,ran6,ran7,count;
   Random ran = new Random();
   //private int [][] Colors;
   private Timer t = new Timer(1000,this);
   private Timer S = new Timer(1500,this);
   private Timer T = new Timer(1500,this);
   private int [][] superColors;
   
   public GameBoardUltra (int boardSize, int numSquares)
	{
		super(boardSize,numSquares);
      numS = numSquares;
      count = 0;
	}
   public void actionPerformed(ActionEvent e) 
   {
      if (e.getSource() == t)
      {
         ran1 = ran.nextInt(numS);
         ran2 = ran.nextInt(numS);
         ran3 = ran.nextInt(4)+1;
         setRanColor(ran1,ran2,ran3);

      }
      else if (e.getSource() == S)
      {
         ran6 = ran.nextInt(numS);
         ran7 = ran.nextInt(numS);
         ran4 = ran6;
         ran5 = ran7;
         setRanColor(ran4,ran5,20);
         T.start();
         S.stop();
         
      }
      else if (e.getSource() == T)
      {
         setRanColor(ran4,ran5,0);
         S.start();
         T.stop();
      }
   }
	public void setRanColor(int row, int col, int ranColor)
   {
      Colors[row][col] = ranColor;
   }
   public void start()
   {
      
      if (count < 1)
      {
         T.start();
         ran1 = ran.nextInt(numS);
         ran2 = ran.nextInt(numS);
         ran3 = ran.nextInt(4)+1;
         setRanColor(ran1,ran2,ran3);
         ran6 = ran.nextInt(numS);
         ran7 = ran.nextInt(numS);
         ran4 = ran6;
         ran5 = ran7;
         setRanColor(ran4,ran5,20);
         count++;
         t.start();
         S.start();
         
      }
   }
   public void stop()
   {
      t.stop();
      S.stop();
      T.stop();
      count=0;
   }
   
   public void ChangeDiff(int SquaresNum)
   {
      SQX = SquaresNum;
      numS = SQX;
		super.repaint();
   }  
}