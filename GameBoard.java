/* GameBoard Class by
     Gaustad, Erik  */

import java.awt.*;
import javax.swing.*;
import java.util.*;
import java.awt.event.*;
//GameBoard class that extends JPanel so it can be placed in a container
//the GameBoard brings in the size from client
public class GameBoard extends JPanel
{
   //variables
	private int X,Y,bSize,SX,SY, num1,numSquares,row,col,ranColor;
	protected int SQX,bSiz;
	protected int [][] Colors;
   private Color darkorange = new Color(254,90,29);
   private Color darkGray = new Color(84,84,84);
   protected ImageIcon image = new ImageIcon("SFGiants.jpg");
   //creates the 2D array in which the color of each square is stored
   //SQX x SQX 2D array that is originally given the value of 0
   //0 equals gray(plain)
   //also sets the start drawing point to 0,0 in the panel
   //also creates variable call certain squares in the 2D array later
	public GameBoard (int boardSize, int numSquares)
	{
		SQX = numSquares;
      
      bSize = boardSize;
		Colors = new int [20][20];
		for (int i=0;i<Colors.length;i++)
		{
			for (int j=0;j<Colors.length;j++)
			{
				Colors[i][j] = 0;
			}
		}
		X = 0;
		Y = 0;
      SX = 0;
      SY = 0;
	}
   //Brings in the color from the client in the form of an int 
   //sets the location in the 2D array equal to that int so...
   //it is repainted with the correct color when repaint() is called
	public void ColorSquare(int color)
	{
		Colors[SY][SX] = color;	
	}
   //returns to the client the int that is stored at the current square
   //this is int is equal to a color
	public int getColor()
	{
		return Colors[SY][SX];
	}
//    public void setRanColor(int row, int col, int ranColor)
//    {
//       Colors[row][col] = ranColor;
//    }
   //creates a move up method which gives the circle a new SY location -1 in the array
   //this new Y location is where the circle is being redrawn with  to create a small space 
   //there is an if statement to make sure the circles isn't moving off of the board
   //it is also setting num1 to 1 if the marker actually moved
   //setting num1 to 0 if it didn't move and returns num1 to the client 
   //also repaints at the end with new location of SX and SY in the 2D array
	public int MoveUp()
	{
		if (!(Y==0))
		{
			Y = Y-(bSize/SQX);
         SY--;
         num1 = 1;
		}
		else
		{
			Y=Y;
         num1 = 0;
		}		
		repaint();
      return num1;
	}
   //creates a moveRight method which gives the circle a new SX location  in the array
   //this new X location is where the circle is being redrawn with  to create a small space 
   //there is an if statement to make sure the circles isn't moving off of the board
   //it is also setting num1 to 1 if the marker actually moved
   //setting num1 to 0 if it didn't move and returns num1 to the client 
   //also repaints at the end with new location of SX and SY in the 2D array
	public int MoveRight()
	{
		if (X<(bSize-(bSize/SQX)-20))
		{
			X = X+(bSize/SQX);
         SX++;
         num1 = 1;
		}
		else
		{
			X=X;
         num1 = 0;
		}		
		repaint();
      return num1;
	}
   //creates a moveLeft method which gives the circle a new SX location -1 in the array
   //this new X location is where the circle is being redrawn with  to create a small space 
   //there is an if statement to make sure the circles isn't moving off of the board
   //it is also setting num1 to 1 if the marker actually moved
   //setting num1 to 0 if it didn't move and returns num1 to the client 
   //also repaints at the end with new location of SX and SY in the 2D array
	public int MoveLeft()
	{
		if (!(X==0))
		{
			X = X-(bSize/SQX);
         SX--;
         num1 = 1;
		}
		else
		{
			X=X;
         num1 = 0;
		}		
		repaint();
      return num1;
	}
   //creates a moveDown method which gives the circle a new SY location  in the array
   //this new Y location is where the circle is being redrawn with  to create a small space 
   //there is an if statement to make sure the circles isn't moving off of the board
   //it is also setting num1 to 1 if the marker actually moved
   //setting num1 to 0 if it didn't move and returns num1 to the client 
   //also repaints at the end with new location of SX and SY in the 2D array
	public int MoveDown()
	{
		if (Y<(bSize-(bSize/SQX)-20))
		{
			Y = Y+(bSize/SQX);
         SY++;
         num1 = 1;
		}
		else
		{
			Y=Y;
         num1 = 0;
		}	
		repaint();
      return num1;
	}
   //creates a resset method to reset everything on the board to it's original starting position
   //and also sets the 2D array back to 0 in every storage spot so there are no colors 
   //then repaints()
	public void Reset()
	{
		X=0;
		Y=0;
      SX = 0;
      SY = 0;
		for (int i=0;i<Colors.length;i++)
		{
			for (int j=0;j<Colors.length;j++)
			{
				Colors[i][j] = 0;
			}
		}
		repaint();
	}
//    public void ChangeDiff(int SquaresNum)
//    {
//       SQX = SquaresNum;
//    }
   //paint method to paint the entire board and then to repaint the entire board 
   //everytime repaint() is called
   //it loops through two for loops to redraw each location in the 2D array with it's color
   //the colors are stored as ints and several if statements are used to decide...
   //what color each square should be colored
   //it then draws that square with appropriate color
   //this method also draws the markerPiece every time at the end
	public void paint (Graphics g) 
   {
      int Xloc = 0;
		int Yloc = 0;    
		super.paint(g);
		for (int i=0;i<SQX;i++)
		{
			for (int j=0;j<SQX;j++)
			{

            if (Colors[i][j] == 1)
				{
					g.setColor(Color.cyan);
				}
				else if (Colors[i][j] == 2)
				{
					g.setColor(Color.yellow);
				}
				else if (Colors[i][j] == 3)
				{
					g.setColor(Color.green);
				}
				else if (Colors[i][j] == 4)
				{
					g.setColor(Color.blue);
				}
				else if (Colors[i][j] == 0)
				{
					g.setColor(darkGray);
				}
            else if (Colors[i][j] == 20)
            {
               g.setColor(darkorange);
            }
				g.fillRect(Xloc,Yloc,bSize/SQX,bSize/SQX);
            g.setColor(Color.gray);
            g.drawRect(Xloc+1,Yloc+1,bSize/SQX-3,bSize/SQX-3);
				Xloc = (Xloc+bSize/SQX);				
			}
			Xloc = 0;
			Yloc = (Yloc+bSize/SQX);			
		}
      g.setColor(darkorange);
		g.fillOval(X,Y,bSize/SQX-1,bSize/SQX-1);
      g.setColor(Color.black);
		g.drawOval(X,Y,bSize/SQX-1,bSize/SQX-1);
      g.setColor(darkGray);
      g.fillOval(X+5,Y+5,bSize/SQX-11,bSize/SQX-11);	
      g.setColor(Color.black);
      g.drawOval(X+5,Y+5,bSize/SQX-11,bSize/SQX-11);
      //image.paintIcon(this,g,X,Y);
	}
}